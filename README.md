# Backoffice Gusto Coffee ☕️

Projet utilisant React ⚛️ et déployé par ElectronJS.

![](https://upload.wikimedia.org/wikipedia/commons/9/91/Electron_Software_Framework_Logo.svg)

## Prérequis 🔧

- Node 10.x.x
- NPM / Yarn

## Installation 🔄

```bash
git clone
```

```bash
cd <projet>
```

```bash
npm install || yarn
```

Modifer les URL de développement en local et déploiement pour l'API dans `/src/info.json`

## Structure 🧱

Même structure qu'un projet React avec un fichier supplémentaire.

```
backoffice
|
└─── public
        electron.js
```

Le fichier `electron.js` permet d'ouvrir le projet dans une fenêtre logicielle.
Récupération de `index.html` dans `build/index.html` pour afficher le projet React dans l'application ElectronJS.

## Lancement 🚀

`npm start`

## Mise en production 📦

Windows : `npm run package-win`
ou Mac : `npm run package-mac`
ou Linux : `npm run package-linux`

L'exécutable se trouve dans le dossier `/release` à la racine du projet.

## Utilisation

## Tests
