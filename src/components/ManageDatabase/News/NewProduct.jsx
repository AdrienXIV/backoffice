import React, { useState, useEffect } from 'react';
import {
  Form,
  Input,
  TextArea,
  Button,
  Message,
  Dropdown,
  Loader,
} from 'semantic-ui-react';
import { Redirect } from 'react-router-dom';
import * as API from '../../../utils/API';

const priceOptions = [
  { key: 'e', value: 'euro', text: '€' },
  { key: 'd', value: 'dollar', text: '$' },
  { key: 'l', value: 'livre', text: '£' },
];

/**
 * Formulaire d'ajout d'un produit en BDD
 */
export default function NewProduct({ type }) {
  // variable bloquante en attendant du chargement de toutes les données
  const [dataLoaded, setDataLoaded] = useState(false);

  // catégories et options disponibles en BDD
  const [categories, setCategories] = useState([]);
  const [options, setOptions] = useState([]);
  // variable du produit à créer
  const [state, setState] = useState({});

  const [redirect, setRedirect] = useState(false);
  // messages erreur/succès
  const [success, setSuccess] = useState(false);
  const [error, setError] = useState(false);
  const [errorContent, setErrorContent] = useState('');

  useEffect(() => {
    // récupérer toutes les catégories
    API.getCategories()
      .then(({ data, status }) => {
        if (status === 200) {
          data.forEach(item => {
            setCategories(prev => [
              ...prev,
              {
                key: item._id.toString(),
                text: item.name,
                value: item._id,
              },
            ]);
          });

          // récupérer toutes les options
          return API.getOptions();
        }
      })
      .then(({ data, status }) => {
        if (status === 200) {
          data.forEach(item => {
            setOptions(prev => [
              ...prev,
              {
                key: item._id.toString(),
                text: item.name,
                value: item._id,
              },
            ]);
          });

          setDataLoaded(true);
        }
      })
      .catch(err => {
        setDataLoaded(true);
        console.log(err);
        alert(err.response.data.error);
      });
  }, [type]);

  if (redirect) return <Redirect to={`/manage-database/${type}`} />;
  else if (!dataLoaded)
    return <Loader active indeterminate content='Chargement' />;
  else
    return (
      <div id='ne [state'>
        <Form onSubmit={handleSubmit} error={error} success={success}>
          <Form.Group widths='equal'>
            {/* NOM DU PRODUIT */}
            <Form.Field required>
              <label>Nom du produit</label>
              <Input
                onChange={handleChange}
                placeholder='Café'
                name='name'
                required
              />
            </Form.Field>

            {/* PRIX DU PRODUIT */}
            <Form.Field required>
              <label>Prix</label>
              {/* MONNAIE */}
              <Input
                required
                label={
                  <Dropdown
                    onChange={handleChange}
                    defaultValue='euro'
                    name='currency'
                    options={priceOptions}
                  />
                }
                onChange={handleChange}
                labelPosition='right'
                step='0.01'
                type='number'
                name='price'
              />
            </Form.Field>
          </Form.Group>

          <Form.Group widths='equal'>
            {/* DESCRIPTION DU PRODUIT */}
            <Form.Field required>
              <label>Description</label>
              <TextArea
                maxLength='250'
                required
                rows={3}
                onChange={handleChange}
                placeholder='Café issu de grains ...'
                name='description'
              />
              <div className='characters-max'>
                <span>250 caractères maximum</span>
              </div>
            </Form.Field>

            <Form.Field>
              <Form.Group widths='equal'>
                {/* OFFRE DU PRODUIT */}
                <Form.Field>
                  <label>Offre</label>
                  <Input
                    onChange={handleChange}
                    placeholder='30'
                    name='offer'
                  />
                </Form.Field>

                {/* POINT DU PRODUIT */}
                <Form.Field>
                  <label>Points</label>
                  <Input
                    onChange={handleChange}
                    placeholder='5'
                    name='points'
                  />
                </Form.Field>
              </Form.Group>
            </Form.Field>
          </Form.Group>

          <Form.Group widths='equal'>
            {/* CATEGORIE DU PRODUIT */}
            <Form.Field required>
              <label>Catégories</label>
              <Dropdown
                required
                name='category_id'
                onChange={handleChange}
                placeholder='Choisir une ou des catégories'
                fluid
                multiple
                selection
                labeled
                search
                options={categories}
              />
            </Form.Field>

            {/* OPTIONS DU PRODUIT */}
            <Form.Field>
              <label>Choix des options pour ce produit</label>
              <Dropdown
                name='option_id'
                onChange={handleChange}
                placeholder='Ajouter ou non des options pour ce produit'
                fluid
                multiple
                selection
                labeled
                search
                options={options}
              />
            </Form.Field>
          </Form.Group>

          <Message success header='Succès' content='Nouveau produit ajouté !' />
          <Message
            error
            header="Erreur lors de l'envoi du formulaire !"
            content={errorContent}
          />

          <Button
            primary
            type='submit'
            content='Ajouter'
            title='Ajouter un nouveau produit'
          />
          <Button
            color='grey'
            content='Annuler'
            title="Annuler l'opération en cours"
            type='button'
            onClick={() => {
              setRedirect(true);
            }}
          />
        </Form>
      </div>
    );

  function handleSubmit(e) {
    // mise à 0 si jamais il y a déjà eu un message
    setError(false);
    setSuccess(false);
    //
    e.preventDefault();
    API.newProduct(state)
      .then(({ status }) => {
        if (status === 201) {
          setSuccess(true); // affichage du message de succès
          setTimeout(() => {
            setRedirect(true); // redirection automatique
          }, 2500);
        }
      })
      .catch(err => {
        console.log(err.response);
        setErrorContent(JSON.stringify(err.response.data.error, null, '\t')); // affichage du contenu du message d'erreur
        setError(true); //  affichage du message d'erreur
      });
  }

  function handleChange(e, { name, value, type }) {
    setState(prevState => ({
      ...prevState,
      [name]: type === 'number' ? parseFloat(value) : value,
    }));
  }
}
