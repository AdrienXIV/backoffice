import React, { useState } from 'react';
import {
  Button,
  Header,
  Form,
  Input,
  Message,
  Dropdown,
} from 'semantic-ui-react';
import * as API from '../../../utils/API';
import { Redirect } from 'react-router-dom';

const priceOptions = [
  { key: 'e', value: 'euro', text: '€' },
  { key: 'd', value: 'dollar', text: '$' },
  { key: 'l', value: 'livre', text: '£' },
];
/**
 * Formulaire d'ajout d'une option en BDD
 */
export default function NewOption({ type }) {
  const [state, setState] = useState({});
  const [redirect, setRedirect] = useState(false);

  // messages
  const [success, setSuccess] = useState(false);
  const [error, setError] = useState(false);
  const [errorContent, setErrorContent] = useState('');

  if (redirect) return <Redirect to={`/manage-database/${type}`} />;
  else
    return (
      <div id='new-option'>
        <Header
          textAlign='center'
          as='h3'
          content={"Ajout d'une nouvelle option"}
        />
        <Form onSubmit={handleSubmit} success={success} error={error}>
          <Form.Group>
            {/* NOM DE L'OPTION' */}
            <Form.Field width='10' required>
              <label>{"Nom de l'option"}</label>
              <Input onChange={handleChange} name='name' required />
            </Form.Field>
            {/* PRIX DE L'OPTION */}
            <Form.Field width='6' required>
              <label>Prix</label>
              {/* MONNAIE */}
              <Input
                required
                label={
                  <Dropdown
                    onChange={handleChange}
                    name='currency'
                    value={priceOptions[0].value}
                    options={priceOptions}
                  />
                }
                onChange={handleChange}
                labelPosition='right'
                step='0.01'
                type='number'
                name='price'
              />
            </Form.Field>
          </Form.Group>
          <Message
            success
            header='Succès'
            content='Nouvelle option ajoutée !'
          />
          <Message
            error
            header="Erreur lors de l'envoi du formulaire !"
            content={errorContent}
          />
          <Button
            primary
            content='Ajouter'
            title='Ajouter une nouvelle option'
            type='submit'
          />
          <Button
            color='grey'
            content='Annuler'
            title="Annuler l'opération en cours"
            type='button'
            onClick={() => {
              setRedirect(true);
            }}
          />
        </Form>
      </div>
    );

  function handleSubmit(e) {
    // mise à 0 si jamais il y a déjà eu un message
    setError(false);
    setSuccess(false);
    //
    e.preventDefault();
    API.newOption(state)
      .then(({ status }) => {
        if (status === 201) {
          setSuccess(true); // affichage du message de succès
          setTimeout(() => {
            setRedirect(true); //redirection automatique
          }, 2500);
        }
      })
      .catch(err => {
        console.log(err.response);
        setErrorContent(JSON.stringify(err.response.data.error, null, '\t')); // affichage du contenu du message d'erreur
        setError(true); //  affichage du message d'erreur
      });
  }

  function handleChange(e, { name, value, type }) {
    setState(prevState => ({
      ...prevState,
      [name]: type === 'number' ? parseFloat(value) : value,
    }));
  }
}
