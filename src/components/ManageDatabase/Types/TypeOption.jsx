import isElectron from 'is-electron';
import React, { useEffect, useState } from 'react';
import DataTable from 'react-data-table-component';
import { Link } from 'react-router-dom';
import { Button, Icon } from 'semantic-ui-react';
import * as API from '../../../utils/API';

const dialog = window.electron.dialog;

/**
 * Affiche tous les items en BDD correspondant à la table "Option"
 */
export default function TypeOption({ type }) {
  const columns = [
    {
      name: "Nom de l'option",
      selector: 'name',
      sortable: true,
      width: '85%',
    },
    {
      sortable: false,
      cell: row => (
        <Link to={`/manage-database/${type}/edit/${row._id}`}>
          <Icon
            title='Modifier'
            size='large'
            link
            color='blue'
            circular
            name='edit'
          />
        </Link>
      ),
      width: '7.5%',
      center: true,
    },
    {
      sortable: false,
      cell: row => (
        <Icon
          title='Supprimer'
          size='large'
          link
          color='red'
          circular
          name='trash'
          onClick={handleClickDelete.bind(this, row.index, row._id)}
        />
      ),
      width: '7.5%',
      center: true,
    },
  ];
  const [state, setState] = useState([]);

  useEffect(() => {
    API.getOptions()
      .then(({ data, status }) => {
        if (status === 200) {
          data.forEach((item, index) => {
            setState(prev => [
              ...prev,
              {
                _id: item._id,
                name: item.name,
                index,
              },
            ]);
          });
        }
      })
      .catch(err => {
        console.log(err.response);
        if (isElectron()) {
          dialog.showErrorBox('Erreur serveur', err.response.data.error);
        } else alert(err.response.data.error);
      });
  }, [type]);

  return (
    <div id='option-type'>
      <Link to={`/manage-database/${type}/new`}>
        <Button
          floated='right'
          primary
          content='Nouvelle option'
          title='Ajouter une nouvelle option'
        />
      </Link>
      <DataTable
        columns={columns}
        data={state}
        dense
        responsive
        striped
        highlightOnHover
        pagination
        paginationRowsPerPageOptions={[10, 25, 50, 100]}
        language={{
          url: '//cdn.datatables.net/plug-ins/1.10.21/i18n/French.json',
        }}
      />
    </div>
  );

  /**
   * Bouton pour supprimer dynamiquement
   */
  function handleClickDelete(index, id) {
    if (isElectron())
      dialog
        .showMessageBox(null, {
          buttons: ['Oui', 'Non'],
          cancelId: 10,
          message:
            'Confirmez-vous la suppression de ' + state[index].name + ' ?',
        })
        .then(({ response }) => {
          if (response === 0)
            API.manageDatabase(id, state, 'DELETE', type)
              .then(({ status }) => {
                if (status === 200) {
                  dialog.showMessageBox(null, {
                    message: 'Suppression réussi !',
                  });
                  window.location.reload();
                }
              })
              .catch(err => {
                console.log(err.response);
                if (isElectron()) {
                  dialog.showErrorBox(
                    'Erreur serveur',
                    err.response.data.error,
                  );
                } else alert(err.response.data.error);
              });
          else return;
        });
    else {
      const r = window.confirm(
        'Confirmez-vous la suppression de ' + state[index].name + ' ?',
      );
      if (r)
        API.manageDatabase(id, state[index], 'DELETE', type)
          .then(({ status }) => {
            if (status === 200) {
              alert('Suppression réussi !');
              window.location.reload();
            }
          })
          .catch(err => {
            console.log(err.response);
            alert(err.response);
          });
    }
  }
}
