import isElectron from 'is-electron';
import React, { useEffect, useState } from 'react';
import DataTable from 'react-data-table-component';
import { Link } from 'react-router-dom';
import { Button, Icon } from 'semantic-ui-react';
import * as API from '../../../utils/API';
import * as STORAGE from '../../../utils/Storage';

const dialog = window.electron.dialog;

/**
 * Seuls les admins peuvent accéder à cette partie
 * Affiche tous les items en BDD correspondant à la table "User" dont les rôles sont admin, manager, employee
 */
export default function TypeEmployee({ type }) {
  const columns = [
    {
      name: 'Prénon',
      selector: 'firstname',
      sortable: true,
      width: '20%',
    },
    {
      name: 'Nom',
      selector: 'lastname',
      sortable: true,
      width: '20%',
    },
    {
      name: 'email',
      selector: 'email',
      sortable: true,
      width: '30%',
    },
    {
      name: 'rôle',
      selector: 'role',
      sortable: true,
      width: '15%',
    },
    {
      sortable: false,
      cell: row => (
        <Link to={`/manage-database/${type}/edit/${row._id}`}>
          <Icon
            title='Modifier'
            size='large'
            link
            color='blue'
            circular
            name='edit'
          />
        </Link>
      ),
      center: true,
      width: '7.5%',
    },
    {
      sortable: false,
      cell: row => (
        <Icon
          title='Supprimer'
          size='large'
          link
          color='red'
          circular
          name='trash'
          onClick={handleClickDelete.bind(this, row.index, row._id)}
        />
      ),
      center: true,
      width: '7.5%',
    },
  ];
  const [state, setState] = useState([]);

  useEffect(() => {
    API.getEmployees()
      .then(({ data, status }) => {
        if (status === 200) {
          data.forEach((item, index) => {
            setState(prev => [
              ...prev,
              {
                _id: item._id,
                firstname: item.firstname,
                lastname: item.lastname,
                email: item.email,
                role: item.role,
                index,
              },
            ]);
          });
        }
      })
      .catch(err => {
        console.log(err.response);
        if (isElectron()) {
          dialog.showErrorBox('Erreur serveur', err.response.data.error);
        } else alert(err.response.data.error);
      });
  }, [type]);

  return (
    <div id='user-type'>
      <Link to={`/manage-database/${type}/new`}>
        <Button
          floated='right'
          primary
          content='Ajouter utilisateur'
          title='Ajouter un nouvel utilisateur, employé'
        />
      </Link>
      <DataTable
        columns={columns}
        data={state}
        dense
        responsive
        striped
        highlightOnHover
        pagination
        paginationRowsPerPageOptions={[10, 25, 50, 100]}
        language={{
          url: '//cdn.datatables.net/plug-ins/1.10.21/i18n/French.json',
        }}
      />
    </div>
  );

  /**
   * Bouton pour supprimer dynamiquement
   */
  function handleClickDelete(index, id) {
    if (isElectron())
      dialog
        .showMessageBox(null, {
          buttons: ['Oui', 'Non'],
          cancelId: 10,
          message: `Confirmez-vous la suppression de ${state[index].firstname} ${state[index].lastname} ?`,
        })
        .then(({ response }) => {
          if (response === 0)
            API.manageDatabase(id, state, 'DELETE', type)
              .then(({ status }) => {
                if (status === 200) {
                  dialog.showMessageBox(null, {
                    message: 'Suppression réussi !',
                  });
                  window.location.reload();
                }
              })
              .catch(err => {
                console.log(err.response);
                if (isElectron()) {
                  dialog.showErrorBox(
                    'Erreur serveur',
                    err.response.data.error,
                  );
                } else alert(err.response.data.error);
              });
          else return;
        });
    else {
      const r = window.confirm(
        `Confirmez-vous la suppression de ${state[index].firstname} ${state[index].lastname} ?`,
      );
      if (r)
        API.manageDatabase(id, state[index], 'DELETE', type)
          .then(({ status }) => {
            if (status === 200) {
              alert('Suppression réussi !');
              // si l'on supprime l'utilisateur courrant, alors on supprime tout son stockage local
              // pour une deconnexion automatique car pas de token
              id === STORAGE.get('id')
                ? STORAGE.clear()
                : window.location.reload();
            }
          })
          .catch(err => {
            console.log(err.response);
            alert(err.response.data.error.message);
          });
    }
  }
}
