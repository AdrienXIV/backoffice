import isElectron from 'is-electron';
import React, { useEffect, useState } from 'react';
import { Redirect } from 'react-router-dom';
import {
  Button,
  Form,
  Header,
  Image,
  Input,
  Loader,
  Message,
  Modal,
  Select,
} from 'semantic-ui-react';
import * as API from '../../../utils/API';
import * as STORAGE from '../../../utils/Storage';

const rolesOptions = [
  { key: 'e', text: 'Employé', value: 'employee' },
  { key: 'm', text: "Chef d'équipe", value: 'manager' },
  { key: 'a', text: 'Admin', value: 'admin' },
  { key: 'c', text: 'Client', value: 'client' },
];

const dialog = window.electron.dialog;

/**
 * Formulaire de modification d'un employé en BDD
 */
export default function EditEmployee({ type, id }) {
  // variable bloquante en attendant du chargement de toutes les données
  const [dataLoaded, setDataLoaded] = useState(false);

  const [state, setState] = useState({});
  const [password, setPassword] = useState({});
  const [redirect, setRedirect] = useState(false);
  const [open, setOpen] = useState(false);

  // messages
  const [success, setSuccess] = useState(false);
  const [error, setError] = useState(false);
  const [errorContent, setErrorContent] = useState('');

  useEffect(() => {
    API.getEmployee(id)
      .then(({ data, status }) => {
        if (status === 200) {
          setState(data);
          setDataLoaded(true);
        }
      })
      .catch(err => {
        setDataLoaded(true);
        console.log(err.response);
        if (isElectron()) {
          dialog.showErrorBox('Erreur serveur', err.response.data.error);
        } else alert(err.response.data.error);
      });
  }, [type, id]);

  if (redirect) {
    return <Redirect to={`/manage-database/${type}`} />;
  } else if (!dataLoaded)
    return <Loader active indeterminate content='Chargement' />;
  else
    return (
      <div id='edit-user'>
        <Header
          textAlign='center'
          as='h3'
          content={
            'Modification du profil : ' + state.firstname + ' ' + state.lastname
          }
          image={
            <Image
              src='https://cdn.pixabay.com/photo/2012/04/26/19/43/profile-42914_1280.png'
              size='massive'
              circular
            />
          }
        />
        <Form onSubmit={handleSubmit} success={success} error={error}>
          <Form.Group widths='equal'>
            {/* PRENOM */}
            <Form.Field required>
              <label>Prénom</label>
              <Input
                required
                onChange={handleChange}
                value={state.firstname}
                name='firstname'
              />
            </Form.Field>
            {/* NOM */}
            <Form.Field required>
              <label>Nom</label>
              <Input
                required
                onChange={handleChange}
                value={state.lastname}
                name='lastname'
              />
            </Form.Field>
          </Form.Group>

          <Form.Group widths='equal'>
            {/* COURRIEL */}
            <Form.Field required>
              <label>Courriel</label>
              <Input
                required
                type='email'
                onChange={handleChange}
                value={state.email}
                name='email'
              />
            </Form.Field>
            <Form.Field>
              <label>Rôle</label>
              <Select
                required
                width='6'
                name='role'
                options={rolesOptions}
                value={state.role}
                onChange={handleChange}
              />
            </Form.Field>
          </Form.Group>

          <Form.Group id='edit-password'>
            <Form.Field>{editPassword()}</Form.Field>
          </Form.Group>
          <Message success header='Succès' content='Modification réussi !' />
          <Message
            error
            header="Erreur lors de l'envoi du formulaire !"
            content={errorContent}
          />
          <Form.Field>
            <Button
              primary
              content='Modifier'
              type='submit'
              title='Modifier le profil'
            />
            <Button
              color='red'
              content='Supprimer le profil'
              title='Supprimer le profil'
              type='button'
              onClick={handleClickDelete}
            />
          </Form.Field>
        </Form>
      </div>
    );

  /**
   * Bouton Modifier dynamiquement les modèles ayant peu de champs comme option, category
   */
  function handleSubmit() {
    // mise à 0 si jamais il y a déjà eu un message
    setError(false);
    setSuccess(false);
    //
    API.manageDatabase(id, state, 'PATCH', type)
      .then(({ status }) => {
        if (status === 201) {
          setSuccess(true); // affichage du message de succès
          setTimeout(() => {
            setRedirect(true); //redirection automatique
          }, 2500);
        }
      })
      .catch(err => {
        console.log(err.response);
        setErrorContent(JSON.stringify(err.response.data.error, null, '\t')); // affichage du contenu du message d'erreur
        setError(true); //  affichage du message d'erreur
      });
  }

  function handleChange(e, { name, value, type }) {
    setState(prevState => ({
      ...prevState,
      [name]: type === 'number' ? parseFloat(value) : value,
    }));
  }

  /**
   * Bouton pour supprimer
   */
  function handleClickDelete() {
    if (isElectron())
      dialog
        .showMessageBox(null, {
          buttons: ['Oui', 'Non'],
          message:
            'Confirmez-vous la suppression de ' +
            state.firstname +
            ' ' +
            state.lastname +
            ' ?',
        })
        .then(({ response }) => {
          if (response === 0)
            API.manageDatabase(state._id, state, 'DELETE', type)
              .then(({ status }) => {
                if (status === 200) {
                  dialog.showMessageBox(null, {
                    message: 'Suppression réussi !',
                  });
                  setRedirect(true); //redirection automatique
                }
              })
              .catch(err => {
                console.log(err.response);
                if (isElectron()) {
                  dialog.showErrorBox(
                    'Erreur serveur',
                    err.response.data.error,
                  );
                } else alert(err.response.data.error);
              });
          else return;
        });
    else {
      const r = window.confirm(
        'Confirmez-vous la suppression de ' +
          state.firstname +
          ' ' +
          state.lastname +
          ' ?',
      );
      if (r)
        API.manageDatabase(state._id, state, 'DELETE', type)
          .then(({ status }) => {
            if (status === 200) {
              alert('Suppression réussi !');
              // si l'on supprime l'utilisateur courrant, alors on supprime tout son stockage local
              // pour une deconnexion automatique car pas de token
              if (id === STORAGE.get('id')) STORAGE.clear();
              setRedirect(true); //redirection automatique;
            }
          })
          .catch(err => {
            console.log(err.response);
            alert(err.response);
          });
    }
  }

  function editPassword() {
    return (
      <Modal
        open={open}
        onClose={() => {
          setOpen(false);
        }}
        closeIcon
        trigger={
          <a
            style={{ cursor: 'pointer' }}
            onClick={() => {
              setOpen(true);
            }}>
            Modifier le mot de passe
          </a>
        }>
        <Modal.Header>Modification du mot de passe</Modal.Header>
        <Modal.Content>
          <Form>
            <Form.Field>
              <label>Ancien mot de passe</label>
              <Input
                type='password'
                name='old_password'
                onChange={passwordChange}
              />
            </Form.Field>
            <Form.Field>
              <label>Nouveau mot de passe</label>
              <Input
                type='password'
                name='new_password'
                onChange={passwordChange}
              />
            </Form.Field>
          </Form>

          <Button
            primary
            style={{ marginTop: '2.5%' }}
            onClick={passwordSubmit}>
            Modifier
          </Button>
        </Modal.Content>
      </Modal>
    );
  }

  function passwordChange(e, { name, value }) {
    setPassword(prevState => ({
      ...prevState,
      [name]: value,
    }));
  }
  function passwordSubmit(e) {
    e.preventDefault();
    API.changePassword(id, password)
      .then(({ status }) => {
        if (status === 201) {
          if (isElectron()) {
            dialog.showMessageBox(null, {
              message: 'Mot de passe changé !',
            });
          } else alert('Mot de passe changé !');
          setOpen(false); // fermeture automatique de la fenêtre modal
        }
      })
      .catch(err => {
        console.log(err.response);
        if (isElectron()) {
          dialog.showErrorBox('Erreur serveur', err.response.data.error);
        } else alert(err.response.data.error);
        setPassword({});
      });
  }
}
