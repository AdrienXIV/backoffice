import isElectron from 'is-electron';
import React, { useEffect, useState } from 'react';
import { Redirect } from 'react-router-dom';
import {
  Button,
  Form,
  Header,
  Input,
  Loader,
  Message,
  Select,
} from 'semantic-ui-react';
import * as API from '../../../utils/API';

const dialog = window.electron.dialog;

/**
 * Formulaire de modification d'une catégorie en BDD
 */
export default function EditLocation({ type, id }) {
  // variable bloquante en attendant du chargement de toutes les données
  const [dataLoaded, setDataLoaded] = useState(false);
  // catégories des emplacements
  const [categories, setCategories] = useState([]);

  const [state, setState] = useState({});
  const [redirect, setRedirect] = useState(false);

  // messages
  const [success, setSuccess] = useState(false);
  const [error, setError] = useState(false);
  const [errorContent, setErrorContent] = useState('');

  useEffect(() => {
    API.getLocationCategories()
      .then(({ data, status }) => {
        if (status === 200) {
          data.forEach(item => {
            setCategories(prev => [
              ...prev,
              {
                key: item._id.toString(),
                text: item.name,
                value: item._id,
              },
            ]);
          });
          return API.getLocation(id);
        }
      })
      .then(({ data, status }) => {
        if (status === 200) {
          console.log(data);
          setState(data);
          setDataLoaded(true);
        }
      })
      .catch(err => {
        setDataLoaded(true);
        console.log(err);
        if (isElectron()) {
          dialog.showErrorBox('Erreur serveur', err.response.data.error);
        } else alert(err.response.data.error);
      });
  }, [type, id]);

  if (redirect) {
    return <Redirect to={`/manage-database/${type}`} />;
  } else if (!dataLoaded)
    return <Loader active indeterminate content='Chargement' />;
  else
    return (
      <div id='edit-location'>
        <Header
          textAlign='center'
          as='h3'
          content={"Modification de l'emplacement : " + state.name}
        />
        <Form onSubmit={handleSubmit} success={success} error={error}>
          <Form.Group widths='equal'>
            {/* NOM DE LA CATEGORIE D'EMPLACEMENT */}
            <Form.Field required>
              <label>{"Nom de l'emplacement"}</label>
              <Input
                onChange={handleChange}
                value={state.name}
                name='name'
                required
              />
            </Form.Field>

            {/* CATEGORIE DE L'EMPLACEMENT */}
            <Form.Field required>
              <label>{"Catégorie de l'emplacement"}</label>
              <Select
                required
                name='location_category_id'
                onChange={handleChange}
                placeholder='Choisir une catégories'
                fluid
                selection
                search
                value={state.location_category_id._id}
                options={categories}
              />
            </Form.Field>
          </Form.Group>

          <Message success header='Succès' content='Modification réussi !' />
          <Message
            error
            header="Erreur lors de l'envoi du formulaire !"
            content={errorContent}
          />

          <Form.Field>
            <Button
              primary
              content='Modifier'
              type='submit'
              title='Modifier la catégorie'
            />
            <Button
              color='red'
              content='Supprimer'
              title='Supprimer la catégorie'
              type='button'
              onClick={handleClickDelete}
            />
          </Form.Field>
        </Form>
      </div>
    );

  /**
   * Bouton Modifier dynamiquement les modèles ayant peu de champs comme option, category
   */
  function handleSubmit() {
    // mise à 0 si jamais il y a déjà eu un message
    setError(false);
    setSuccess(false);
    //
    API.manageDatabase(id, state, 'PATCH', type)
      .then(({ status }) => {
        if (status === 201) {
          setSuccess(true); // affichage du message de succès
          setTimeout(() => {
            setRedirect(true); //redirection automatique
          }, 2500);
        }
      })
      .catch(err => {
        console.log(err.response);
        setErrorContent(JSON.stringify(err.response.data.error, null, '\t')); // affichage du contenu du message d'erreur
        setError(true); //  affichage du message d'erreur
      });
  }

  function handleChange(e, { name, value }) {
    setState(prevState => ({ ...prevState, [name]: value }));
  }

  /**
   * Bouton pour supprimer
   */
  function handleClickDelete() {
    if (isElectron())
      dialog
        .showMessageBox(null, {
          buttons: ['Oui', 'Non'],
          message: 'Confirmez-vous la suppression de ' + state.name + ' ?',
        })
        .then(({ response }) => {
          if (response === 0)
            API.manageDatabase(state._id, state, 'DELETE', type)
              .then(({ status }) => {
                if (status === 200) {
                  dialog.showMessageBox(null, {
                    message: 'Suppression réussi !',
                  });
                  setRedirect(true); //redirection automatique
                }
              })
              .catch(err => {
                console.log(err.response);
                if (isElectron()) {
                  dialog.showErrorBox(
                    'Erreur serveur',
                    err.response.data.error,
                  );
                } else alert(err.response.data.error);
              });
          else return;
        });
    else {
      const r = window.confirm(
        'Confirmez-vous la suppression de ' + state.name + ' ?',
      );
      if (r)
        API.manageDatabase(state._id, state, 'DELETE', type)
          .then(({ status }) => {
            if (status === 200) {
              alert('Suppression réussi !');
              setRedirect(true); //redirection automatique
            }
          })
          .catch(err => {
            console.log(err.response);
            alert(err.response);
          });
    }
  }
}
