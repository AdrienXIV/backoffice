import isElectron from 'is-electron';
import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import { Button, Header, Label, List, Table } from 'semantic-ui-react';
import * as API from '../../utils/API';
import { socket as io } from '../../utils/SocketIO';
const dialog = window.electron.dialog;

/**
 * Produits de la commande
 * @param {Object} order
 * @param {String} total_price
 * @param {Number} id
 * @param {Boolean} isPrepared
 */
export default function OrderProduct() {
  const [orders, setOrders] = useState([]);
  console.log('orders:', orders);
  const [socket, setSocket] = useState(null);
  useEffect(() => {
    setSocket(io);
    // demande au chargement de la page la récupération des commandes payées
    API.getOrdersPaid()
      .then(({ data, status }) => {
        if (status === 200) {
          setOrders(data);
        }
      })
      .catch(err => {
        console.error(err.response);
        if (isElectron()) {
          dialog.showErrorBox(
            'Erreur lors de la récupération des commandes',
            err.response.data.error,
          );
        } else alert(err.response.data.error);
      });
  }, []);

  useEffect(() => {
    if (!socket) return;

    // réception des commandes payées
    socket.on('orders-paid', data => {
      setOrders(prev => [...prev, data]);
    });
  }, [socket]);

  return orders.map((order, index) =>
    order.articles.length !== 0 ? (
      <Table.Row key={'orderproduct' + index.toString()}>
        <Table.Cell textAlign='center'>
          <Header>
            <Link to={'/order/' + order._id}>{order.id_order}</Link>
          </Header>
        </Table.Cell>
        {/* pas d'alignement centré pour les produits */}
        <Table.Cell>
          <List>{showArticles(order.articles)}</List>
        </Table.Cell>
        <Table.Cell textAlign='center'>
          <List>{showQuantity(order.articles)}</List>
        </Table.Cell>

        <Table.Cell textAlign='center'>
          <List>{showPrice(order.articles)}</List>
        </Table.Cell>

        <Table.Cell textAlign='center'>
          <Label
            content={totalPriceWithoutReservations(order.articles) + '€'}
            color='brown'
            size='big'
          />
        </Table.Cell>

        <Table.Cell textAlign='center'>
          <Button
            size='big'
            inverted
            color='green'
            onClick={updateStatus.bind(this, order._id)}
            content='Confirmer préparation'
            title='Confirmer la préparation'></Button>
        </Table.Cell>
      </Table.Row>
    ) : null,
  );

  function totalPriceWithoutReservations(articles) {
    let totalPrice = 0;
    articles.forEach(
      article => (totalPrice += article.product_price * article.quantity),
    );
    return totalPrice;
  }
  /**
   * Afficher chaque articles (produit + options) de la commande
   * @param {Array} articles
   */
  function showArticles(articles) {
    return articles.map((item, index) => (
      <List.Item key={'productlist' + index.toString()} className='products'>
        <List.Icon name='coffee' />
        <List.Content>
          <List.Header>{item.name}</List.Header>
          {showArticleOptions(item.options)}
        </List.Content>
      </List.Item>
    ));
  }

  /**
   * Afficher la quantité de chaque articles de la commande
   * @param {Array} articles
   */
  function showQuantity(articles) {
    return articles.map((item, index) => (
      <List.Item key={'quantitylist' + index.toString()} className='products'>
        <List.Content>
          <List.Header>{item.quantity}</List.Header>
          <div style={{ filter: 'opacity(0)' }}>
            {showArticleOptions(item.options)}
          </div>
        </List.Content>
      </List.Item>
    ));
  }

  /**
   * Afficher les options du produit de chaque articles de la commande
   * @param {Array} options
   */
  function showArticleOptions(options) {
    return options.map((opt, index) => (
      <List.List key={'productoption' + index.toString()}>
        <List.Item>
          <List.Icon name='add' size='small' />
          <List.Content>{opt.name}</List.Content>
        </List.Item>
      </List.List>
    ));
  }

  /**
   * Afficher le prix de chaque articles d'une commande
   * @param {Array} product
   */
  function showPrice(articles) {
    return articles.map((item, index) => (
      <List.Item key={'prix' + index.toString()} className='prices'>
        <List.Content>
          <List.Header>{item.product_price}€</List.Header>
          {showPriceOptions(item.options)}
        </List.Content>
      </List.Item>
    ));
  }

  /**
   * Afficher le prix des options de chaque produits de la commande
   * @param {Array} options
   */
  function showPriceOptions(options) {
    return options.map((item, index) => (
      <List.List key={'priceoption' + index.toString()}>
        <List.Item>
          <List.Content>{item.option_price}€</List.Content>
        </List.Item>
      </List.List>
    ));
  }

  function updateStatus(_id, e) {
    e.preventDefault();
    let data = {
      _id,
      status: 2,
    };

    socket.emit('update-order-status', data, response => {
      if (response === 201) {
        setOrders(orders.filter(item => item._id !== _id));
      }
    });
  }
}
