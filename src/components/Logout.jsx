import React from 'react';
import * as STORAGE from '../utils/Storage';
import { Redirect } from 'react-router-dom';

export default function Logout() {
  STORAGE.clear();
  return <Redirect to='/'></Redirect>;
}
