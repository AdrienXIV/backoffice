/**
 *
 * @param {Database DateTime} datetime
 * @returns {String} value format : day month year hh:mm:ss
 */
export function dateConverter(date: Date): string {
  const options = {
    weekday: 'long',
    year: 'numeric',
    month: 'long',
    day: 'numeric',
    hour: 'numeric',
    minute: 'numeric',
    second: 'numeric',
  };
  return new Date(date).toLocaleDateString('fr-FR', options);
}
