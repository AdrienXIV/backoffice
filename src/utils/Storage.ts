export function set(name: string, value: any): void {
  localStorage.setItem(name, JSON.stringify(value));
  return;
}
export function get(name: string): any {
  return JSON.parse(localStorage.getItem(name)!);
}
export function clear(): void {
  localStorage.clear();
}
