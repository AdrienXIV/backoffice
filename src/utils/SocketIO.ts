import io from 'socket.io-client';
import { url_api as __URL } from '../info.json';
import * as STORAGE from './Storage';

// options de la connexion
const __OPTIONS: Object | undefined = {
  query: { token: STORAGE.get('token'), room: 'NWS2020' },
  secure: true,
};

export const socket = io(__URL, __OPTIONS);
