import React from 'react';
import {
  Route,
  Switch,
  HashRouter as Router,
  Redirect,
  Link,
} from 'react-router-dom';
import LoginPage from './pages/Login.page';
import OrderPage from './pages/Order/Order.page';
import * as STORAGE from './utils/Storage';
import Logout from './components/Logout';
import TypePage from './pages/ManageDatabase/Type.page';
import EditPage from './pages/ManageDatabase/Edit.page';
import NewPage from './pages/ManageDatabase/New.page';
import ManageDatabasePage from './pages/ManageDatabase/ManageDatabase.page';
import OrderId from './pages/Order/OrderId.page';
import OrderHistoryPage from './pages/Order/OrderHistory.page';

const ErrorPage = error => {
  return (
    <div>
      <h2>Erreur :</h2>
      <p>{error}</p>
      <Link to='/'>Redirection vers la page de connexion</Link>
    </div>
  );
};
/**
 * Route protégée pour les administrateurs
 * Vérification dans le header avec le token
 */
const AdminRoute = ({ ...props }) => {
  try {
    return STORAGE.get('role') !== 'admin' ? (
      <Redirect to='/' />
    ) : (
      <Route {...props} />
    );
  } catch (err) {
    console.error('error', err.response);
    return <ErrorPage error={err.response} />;
  }
};
/**
 * Route protégée pour les administrateurs ET chefs d'équipe
 * Vérification dans le header avec le token
 */
const AdminAndManagerRoute = ({ ...props }) => {
  try {
    return STORAGE.get('role') !== ('admin' || 'manager') ? (
      <Redirect to='/' />
    ) : (
      <Route {...props} />
    );
  } catch (err) {
    console.error('error', err.response);
    return <ErrorPage error={err.response} />;
  }
};

export default function App() {
  return (
    <Router basename='/'>
      <Switch>
        <Route exact path='/' component={LoginPage} />
        <Route exact path='/logout' component={Logout} />
        {/* Routes protégées pour les admins et chefs d'équipe */}
        <AdminAndManagerRoute exact path='/order' component={OrderPage} />
        <AdminAndManagerRoute exact path='/order/:id' component={OrderId} />
        {/* Routes protégées pour les admins */}
        <AdminRoute
          exact
          path='/manage-database'
          component={ManageDatabasePage}
        />
        <AdminRoute exact path='/manage-database/:type' component={TypePage} />
        <AdminRoute
          exact
          path='/manage-database/:type/new'
          component={NewPage}
        />
        <AdminRoute
          exact
          path='/manage-database/:type/edit/:id'
          component={EditPage}
        />
        <AdminRoute exact path='/order-history' component={OrderHistoryPage} />
      </Switch>
    </Router>
  );
}
