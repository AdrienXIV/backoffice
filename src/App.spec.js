const { url_localhost } = require('./info.json');
const axios = require('axios');

/**
 * TEST DES LIBRAIRIES INCLUES DANS LE index.html
 */
describe('Librairies', () => {
  test('jquery js', async () => {
    const response = await axios.request({
      url: 'https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js',
      method: 'GET',
    });
    expect(response.data).toBeDefined();
  });
  test('semanticui js', async () => {
    const response = await axios.request({
      url:
        'https://cdn.jsdelivr.net/npm/semantic-ui@2.4.2/dist/semantic.min.js',
      method: 'GET',
    });
    expect(response.data).toBeDefined();
  });
  test('semanticui css', async () => {
    const response = await axios.request({
      url:
        'https://cdn.jsdelivr.net/npm/semantic-ui@2.4.2/dist/semantic.min.css',
      method: 'GET',
    });
    expect(response.data).toBeDefined();
  });
});

/**
 * TEST DES REQUETES VERS L'API
 */
describe('Récupérer les produits', () => {
  test('getCategories', async () => {
    const response = await axios.request({
      url: url_localhost + '/category',
      method: 'GET',
    });
    expect(response.status).toEqual(200);
  });
  test('getOptions', async () => {
    const response = await axios.request({
      url: url_localhost + '/option',
      method: 'GET',
    });
    expect(response.status).toEqual(200);
  });
  test('getProducts', async () => {
    const response = await axios.request({
      url: url_localhost + '/product',
      method: 'GET',
    });
    expect(response.status).toEqual(200);
  });
});
