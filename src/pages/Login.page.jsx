import isElectron from 'is-electron';
import React, { useState } from 'react';
import { Redirect } from 'react-router-dom';
import { Button, Form, Grid, Input, Segment } from 'semantic-ui-react';
import * as API from '../utils/API';
import * as STORAGE from '../utils/Storage';

const dialog = window.electron.dialog;

export default function LoginPage() {
  const [state, setState] = useState({
    email: '',
    password: '',
  });
  const [redirect, setRedirect] = useState(false);

  if (redirect) return <Redirect to={`/order`} />;
  else
    return (
      <div id='login-page'>
        <h1 className='title'>
          Gusto coffee - Interface d&apos;administration
        </h1>
        <Segment placeholder>
          <Grid stackable>
            <Grid.Column>
              <Form onSubmit={handleSubmit}>
                <Form.Field>
                  <label>Courriel</label>
                  <Input
                    id='email'
                    name='email'
                    icon='user'
                    iconPosition='left'
                    placeholder='email@gmail.com'
                    value={state.email}
                    type='email'
                    onChange={handleChange}
                  />
                </Form.Field>
                <Form.Field>
                  <label>Mot de passe</label>
                  <Input
                    id='password'
                    name='password'
                    icon='lock'
                    iconPosition='left'
                    type='password'
                    value={state.password}
                    onChange={handleChange}
                  />
                </Form.Field>
                <Button content='Se connecter' positive size='large' />
              </Form>
            </Grid.Column>
          </Grid>
        </Segment>
      </div>
    );

  function handleChange(e, { value, name }) {
    setState(prevState => ({ ...prevState, [name]: value }));
  }

  function handleSubmit(e) {
    e.preventDefault();
    API.login(state.email, state.password)
      .then(({ data, status }) => {
        if (status === 200) {
          STORAGE.set('token', data.user.token);
          STORAGE.set(
            'employee',
            data.user.firstname + ' ' + data.user.lastname,
          );
          STORAGE.set('role', data.user.role);
          STORAGE.set('avatar', data.user.avatar);
          STORAGE.set('id', data.user.id);
          setRedirect(true);
        }
      })
      .catch(err => {
        console.error(err.response);
        if (isElectron()) {
          dialog.showErrorBox(
            'Erreur lors de la connexion',
            err.response.data.error,
          );
        } else alert(err.response.data.error);
        setState(prevState => ({ ...prevState, password: '' }));
      });
  }
}
