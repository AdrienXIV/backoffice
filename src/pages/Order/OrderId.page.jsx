import isElectron from 'is-electron';
import React, { useEffect, useState } from 'react';
import { Link, Redirect } from 'react-router-dom';
import {
  Button,
  Container,
  Header,
  Icon,
  Label,
  Loader,
  Table,
} from 'semantic-ui-react';
import Navbar from '../../components/Navbar';
import * as API from '../../utils/API';
import { dateConverter } from '../../utils/DateConverter';

const dialog = window.electron.dialog;

/**
 * Page d'affichage d'une commande spécifique
 * @param {*} props
 */
export default function OrderId({ match }) {
  const [order, setOrder] = useState({});
  const [color, setColor] = useState('red'); // couleur du tableau
  const [dataLoaded, setDataLoaded] = useState(false);

  useEffect(() => {
    API.getOrder(match.params.id)
      .then(({ data, status }) => {
        if (status === 200) {
          setOrder(data);
          // mise à jour de la couleur du tableau en fonction de l'état de la commande
          if (data.status >= 2)
            if (data.status === 3) setColor('green');
            else setColor('orange');
          else setColor('red');
          //
          setDataLoaded(true);
        }
      })
      .catch(err => {
        console.log(err);
        setDataLoaded(true);
        alert(err);
      });
  }, [match]);

  if (!dataLoaded) return <Loader active indeterminate content='Chargement' />;
  // s'il n'y a pas d'articles (erreur par exemple) on redirige vers la page d'accueil
  if (order.articles === undefined) return <Redirect to='/order' />;

  return (
    <div id='order-id'>
      <Navbar />
      <div className='return'>
        <Link to='/order'>
          <Icon name='arrow left' color='blue' size='big' title='Retour' />
        </Link>
      </div>

      <Container text>
        {/* Titre du tableau en fonction de l'état de la commande */}
        {isPrepared()}

        <Table unstackable color={color}>
          <Table.Header>
            <Table.Row>
              <Table.HeaderCell>Produit</Table.HeaderCell>
              <Table.HeaderCell>Option</Table.HeaderCell>
              <Table.HeaderCell>Quantité</Table.HeaderCell>
              <Table.HeaderCell textAlign='right'>Prix</Table.HeaderCell>
            </Table.Row>
          </Table.Header>

          <Table.Body>
            {order.articles.map((article, index) => (
              <Table.Row key={'orderidrow' + index.toString()}>
                <Table.Cell>
                  {article.product_id.name} ({article.product_id.price}
                  €)
                </Table.Cell>
                <Table.Cell>{showOptions(article.option_ids)}</Table.Cell>
                <Table.Cell>1</Table.Cell>
                <Table.Cell
                  textAlign='right'
                  style={{
                    fontWeight: 'bold',
                  }}>
                  {article.price}€
                </Table.Cell>
              </Table.Row>
            ))}
          </Table.Body>
        </Table>
        <div className='total_price'>
          <Label
            color='brown'
            size='large'
            content={`Total : ${order.total_price}€`}
          />
        </div>

        {showButtonCancerPreparation()}
      </Container>
    </div>
  );

  function showButtonCancerPreparation() {
    return order.status !== 1 ? (
      <Button
        name='is_prepared'
        primary
        content='Annuler préparation'
        title='Annuler la préparation de la commande'
        onClick={updateStatus.bind(this, match.params.id)}
      />
    ) : null;
  }

  function showOptions(options) {
    return options.map((option, index) => (
      <div key={'orderidoption' + index.toString()}>
        {option.name} ({option.price}€)
      </div>
    ));
  }

  function isPrepared() {
    if (order.status >= 2)
      // si la commande est préparée, on vérifie si elle est livrée
      // on convertit la date ici afin que toutes les fonctions qui vont être appelées aient la date en string
      return isDelivered();
    // si elle n'est pas préparee
    else
      return (
        <Header as='h2'>
          <Icon color='red' name='ordered list' />
          <Header.Content>
            Commande{' '}
            <span
              style={{
                color: 'blue',
              }}>
              {order.id_order}
            </span>
            <Header.Subheader>Non préparée</Header.Subheader>
          </Header.Content>
        </Header>
      );
  }

  function isDelivered() {
    if (order.status === 3)
      return (
        <Header as='h2'>
          <Icon color='green' name='ordered list' />
          <Header.Content>
            Commande{' '}
            <span
              style={{
                color: 'blue',
              }}>
              {order.id_order}
            </span>
            <Header.Subheader>
              Livrée :{' '}
              <span
                style={{
                  color: 'silver',
                }}>
                {dateConverter(order.date.updated)}
              </span>
            </Header.Subheader>
          </Header.Content>
        </Header>
      );
    else
      return (
        <Header as='h2'>
          <Icon color='orange' name='ordered list' />
          <Header.Content>
            Commande{' '}
            <span
              style={{
                color: 'blue',
              }}>
              {order.id_order}
            </span>
            <Header.Subheader>
              Préparée le {dateConverter(order.date.updated)}
            </Header.Subheader>
          </Header.Content>
        </Header>
      );
  }

  // changer les status de la commande si l'employé à fait une erreur par exemple
  function updateStatus(id, e, { name }) {
    e.preventDefault();
    let data = {
      // si elle rest préparée elle passe de 2 à 1
      // si elle est livrée elle passe de 3 à 2
      status: name === 'is_prepared' ? 1 : 2,
    };
    if (isElectron())
      dialog
        .showMessageBox(null, {
          buttons: ['Oui', 'Non'],
          cancelId: 10,
          message:
            name === 'is_prepared'
              ? "Confirmez-vous l'annulation de la préparation ?"
              : "Confirmez-vous l'annulation de la livraison ?",
        })
        .then(({ response }) => {
          if (response === 0)
            API.updateOrderStatus(id, data)
              .then(({ status }) => {
                if (status === 201) window.location.reload();
              })
              .catch(err => {
                console.log(err.response);
                dialog.showErrorBox(
                  'Erreur serveur',
                  err.response.data.error.message,
                );
              });
          else return;
        });
    else {
      let r =
        name === 'is_prepared'
          ? window.confirm("Confirmez-vous l'annulation de la préparation ?")
          : window.confirm("Confirmez-vous l'annulation de la livraison ?");

      if (r)
        API.updateOrderStatus(id, data)
          .then(({ status }) => {
            if (status === 201) window.location.reload();
          })
          .catch(err => {
            console.log(err.response);
            alert(err.response.data.error.message);
          });
    }
  }
}
