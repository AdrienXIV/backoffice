import React from 'react';
import { Grid, Segment, Table } from 'semantic-ui-react';
import Navbar from '../../components/Navbar';
import OrderPrepared from '../../components/Orders/OrderPrepared';
import OrderProduct from '../../components/Orders/OrderProducts';

/**
 * Page des commandes en préparation + livrées
 */
export default function OrderPage() {
  return (
    <div id='order-page'>
      <Navbar activeItem='order' />

      <Grid stackable columns={2} padded>
        <Grid.Column width={12}>
          <h2>Commandes en préparation</h2>
          <Segment>
            <Table size='large' basic='very' celled striped>
              <Table.Header>
                <Table.Row textAlign='center'>
                  <Table.HeaderCell>Commande</Table.HeaderCell>
                  <Table.HeaderCell>Produits</Table.HeaderCell>
                  <Table.HeaderCell>Quantité</Table.HeaderCell>
                  <Table.HeaderCell>Prix unitaire</Table.HeaderCell>
                  <Table.HeaderCell>Prix total</Table.HeaderCell>
                  <Table.HeaderCell>
                    Appuyez pour confirmer la préparation
                  </Table.HeaderCell>
                </Table.Row>
              </Table.Header>

              <Table.Body>
                <OrderProduct />
              </Table.Body>
            </Table>
          </Segment>
        </Grid.Column>

        <Grid.Column width={4}>
          <h2>Commandes préparées</h2>
          <Segment textAlign='right'>
            <OrderPrepared />
          </Segment>
        </Grid.Column>
      </Grid>
    </div>
  );
}
