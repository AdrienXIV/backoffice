import React from 'react';
import { Link } from 'react-router-dom';
import Navbar from '../../components/Navbar';
import { Icon, Container } from 'semantic-ui-react';
import EditProduct from '../../components/ManageDatabase/Edits/EditProduct';
import EditCategory from '../../components/ManageDatabase/Edits/EditCategory';
import EditOption from '../../components/ManageDatabase/Edits/EditOption';
import EditEmployee from '../../components/ManageDatabase/Edits/EditEmployee';
import EditLocationCategory from '../../components/ManageDatabase/Edits/EditLocationCategory';
import EditLocation from '../../components/ManageDatabase/Edits/EditLocation';

/**
 * Page des commandes en préparation + livrées
 */
export default function Editpage({ match }) {
  return (
    <div id='edit-page'>
      <Navbar activeItem='db' />
      <div className='return'>
        <Link to={'/manage-database/' + match.params.type}>
          <Icon name='arrow left' color='blue' size='big' title='Retour' />
        </Link>
      </div>
      <Container>{choice()}</Container>
    </div>
  );

  function choice() {
    switch (match.params.type) {
      case 'category':
        return <EditCategory type={match.params.type} id={match.params.id} />;
      case 'product':
        return <EditProduct type={match.params.type} id={match.params.id} />;
      case 'option':
        return <EditOption type={match.params.type} id={match.params.id} />;
      case 'user': // case user car les employés sont dans la même table que les utilisateurs mais avec un rôle différent
        return <EditEmployee type={match.params.type} id={match.params.id} />;
      case 'coworking':
        return (
          <EditLocationCategory type={match.params.type} id={match.params.id} />
        );
      case 'location':
        return <EditLocation type={match.params.type} id={match.params.id} />;
      default:
        break;
    }
  }
}
