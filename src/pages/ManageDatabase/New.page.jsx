import React from 'react';
import { Link } from 'react-router-dom';
import Navbar from '../../components/Navbar';
import { Icon, Container } from 'semantic-ui-react';
import NewCategory from '../../components/ManageDatabase/News/NewCategory';
import NewProduct from '../../components/ManageDatabase/News/NewProduct';
import NewOption from '../../components/ManageDatabase/News/NewOption';
import NewEmployee from '../../components/ManageDatabase/News/NewEmployee';
import NewLocationCategory from '../../components/ManageDatabase/News/NewLocationCategory';
import NewLocation from '../../components/ManageDatabase/News/NewLocation';

/**
 *
 */
export default function NewPage({ match }) {
  return (
    <div id='new-page'>
      <Navbar activeItem='db' />
      <div className='return'>
        <Link to={'/manage-database/' + match.params.type}>
          <Icon name='arrow left' color='blue' size='big' title='Retour' />
        </Link>
      </div>
      <Container>{choice()}</Container>
    </div>
  );

  function choice() {
    switch (match.params.type) {
      case 'category':
        return <NewCategory type={match.params.type} />;
      case 'product':
        return <NewProduct type={match.params.type} />;
      case 'option':
        return <NewOption type={match.params.type} />;
      case 'user': // case user car les employés sont dans la même table que les utilisateurs mais avec un rôle différent
        return <NewEmployee type={match.params.type} />;
      case 'coworking':
        return <NewLocationCategory type={match.params.type} />;
      case 'location':
        return <NewLocation type={match.params.type} />;
      default:
        break;
    }
  }
}
