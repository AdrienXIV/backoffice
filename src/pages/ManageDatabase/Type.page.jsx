import React from 'react';
import Navbar from '../../components/Navbar';
import { Link } from 'react-router-dom';
import { Icon, Container } from 'semantic-ui-react';
import TypeCategory from '../../components/ManageDatabase/Types/TypeCategory';
import TypeProduct from '../../components/ManageDatabase/Types/TypeProduct';
import TypeOption from '../../components/ManageDatabase/Types/TypeOption';
import TypeEmployee from '../../components/ManageDatabase/Types/TypeEmployee';
import TypeLocationCategory from '../../components/ManageDatabase/Types/TypeLocationCategory';
import TypeLocation from '../../components/ManageDatabase/Types/TypeLocation';

/**
 * Page du choix
 */
export default function TypePage({ match }) {
  return (
    <div id='type-page'>
      <Navbar activeItem='db' />
      <div className='return'>
        <Link to={'/manage-database'}>
          <Icon name='arrow left' color='blue' size='big' title='Retour' />
        </Link>
      </div>
      <Container>{choice(match.params.type)}</Container>
    </div>
  );
}
function choice(type) {
  switch (type) {
    case 'category':
      return <TypeCategory type={type} />;
    case 'product':
      return <TypeProduct type={type} />;
    case 'option':
      return <TypeOption type={type} />;
    case 'user': // case user car les employés sont dans la même table que les utilisateurs mais avec un rôle différent
      return <TypeEmployee type={type} />;
    case 'coworking':
      return <TypeLocationCategory type={type} />;
    case 'location':
      return <TypeLocation type={type} />;
    default:
      break;
  }
}
